package main

import (
    "flag"
    "fmt"
    "net"
    "os"
    "strings"
)

var (
    ipv4    bool
    ipv6    bool
    mac     bool
    name    bool
)

func init() {
    flag.BoolVar(&ipv4, "4", false, "Show only IPv4 addresses")
    flag.BoolVar(&ipv6, "6", false, "Show only IPv6 addresses")
    flag.BoolVar(&mac, "m", false, "Include MAC addresses (implies -n)")
    flag.BoolVar(&name, "n", false, "Include interface name")

    flag.Parse()

    if ipv4 && ipv6 {
        fmt.Println("-4 and -6 are mutually exclusive")
        os.Exit(1)
    } else if !ipv4 && !ipv6 {
        ipv4 = true
        ipv6 = true
    }

    // showing MACs implies showing names
    if mac {
        name = true
    }
}

func main() {
    var (
        hasAddr bool
        first bool
    )

    ifaces, _ := net.Interfaces()
    for _, iface := range ifaces {
        // skip loopback
        if string(iface.HardwareAddr) == "" {
            continue
        }

        hasAddr = false
        first = true

        addrs, _ := iface.Addrs()
        for _, addr := range addrs {
            ip := addr.String()

            // determine if we have an IP address
            hasAddr = (ipv4 && strings.Index(ip, ".") != -1) || (ipv6 && strings.Index(ip, ":") != -1)

            if !hasAddr {
                continue
            }

            // print a header with the interface name
            if name && first {
                if mac {
                    fmt.Printf("%s (MAC: %s)\n", iface.Name, iface.HardwareAddr)
                } else {
                    fmt.Println(iface.Name)
                }
                first = false
            }

            if name {
                fmt.Println("  ", ip)
            } else {
                fmt.Println(ip)
            }
        }
    }
}
